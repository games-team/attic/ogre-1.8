Description: drop stack alignment hack that breaks with gcc-4.7
Bug: http://www.ogre3d.org/mantis/view.php?id=553
Bug-Debian: http://bugs.debian.org/687013
Bug-Ubuntu: https://bugs.launchpad.net/ubuntu/+source/ogre-1.8/+bug/1041934
Origin: upstream, Origin: upstream, http://hg.libsdl.org/SDL/rev/62ff1c0a103f
Applied-Upstream: 1.8.1

# HG changeset patch
# User Holger Frydrych <h.frydrych@gmx.de>
# Date 1343895642 -7200
# Branch v1-8
# Node ID 9db0902fcff8772b5f9f6c3459430bb8a75bbe09
# Parent  5bcdaa1cf83322d5a281d1fd6752228a94d51620
Replace an ugly hack for GCC that was trying to force 16-byte stack alignment in OgreOptimisedUtilSSE, but causes trouble with recent GCC versions

--- a/OgreMain/src/OgreOptimisedUtilSSE.cpp
+++ b/OgreMain/src/OgreOptimisedUtilSSE.cpp
@@ -84,7 +84,7 @@
         OptimisedUtilSSE(void);
 
         /// @copydoc OptimisedUtil::softwareVertexSkinning
-        virtual void softwareVertexSkinning(
+        virtual void __OGRE_SIMD_ALIGN_ATTRIBUTE softwareVertexSkinning(
             const float *srcPosPtr, float *destPosPtr,
             const float *srcNormPtr, float *destNormPtr,
             const float *blendWeightPtr, const unsigned char* blendIndexPtr,
@@ -96,7 +96,7 @@
             size_t numVertices);
 
         /// @copydoc OptimisedUtil::softwareVertexMorph
-        virtual void softwareVertexMorph(
+        virtual void __OGRE_SIMD_ALIGN_ATTRIBUTE softwareVertexMorph(
             Real t,
             const float *srcPos1, const float *srcPos2,
             float *dstPos,
@@ -105,28 +105,28 @@
 			bool morphNormals);
 
         /// @copydoc OptimisedUtil::concatenateAffineMatrices
-        virtual void concatenateAffineMatrices(
+        virtual void __OGRE_SIMD_ALIGN_ATTRIBUTE concatenateAffineMatrices(
             const Matrix4& baseMatrix,
             const Matrix4* srcMatrices,
             Matrix4* dstMatrices,
             size_t numMatrices);
 
         /// @copydoc OptimisedUtil::calculateFaceNormals
-        virtual void calculateFaceNormals(
+        virtual void __OGRE_SIMD_ALIGN_ATTRIBUTE calculateFaceNormals(
             const float *positions,
             const EdgeData::Triangle *triangles,
             Vector4 *faceNormals,
             size_t numTriangles);
 
         /// @copydoc OptimisedUtil::calculateLightFacing
-        virtual void calculateLightFacing(
+        virtual void __OGRE_SIMD_ALIGN_ATTRIBUTE calculateLightFacing(
             const Vector4& lightPos,
             const Vector4* faceNormals,
             char* lightFacings,
             size_t numFaces);
 
         /// @copydoc OptimisedUtil::extrudeVertices
-        virtual void extrudeVertices(
+        virtual void __OGRE_SIMD_ALIGN_ATTRIBUTE extrudeVertices(
             const Vector4& lightPos,
             Real extrudeDist,
             const float* srcPositions,
--- a/OgreMain/src/OgreSIMDHelper.h
+++ b/OgreMain/src/OgreSIMDHelper.h
@@ -47,48 +47,18 @@
 // For intel's compiler, simply calling alloca seems to do the right
 // thing. The size of the allocated block seems to be irrelevant.
 #define __OGRE_SIMD_ALIGN_STACK()   _alloca(16)
+#define __OGRE_SIMD_ALIGN_ATTRIBUTE
 
-#elif OGRE_CPU == OGRE_CPU_X86 && (OGRE_COMPILER == OGRE_COMPILER_GNUC || OGRE_COMPILER == OGRE_COMPILER_CLANG)
-//
-// Horrible hack to align the stack to a 16-bytes boundary for gcc.
-//
-// We assume a gcc version >= 2.95 so that
-// -mreferred-stack-boundary works.  Otherwise, all bets are
-// off.  However, -mreferred-stack-boundary does not create a
-// stack alignment, but it only preserves it.  Unfortunately,
-// since Ogre are designed as a flexibility library, user might
-// compile their application with wrong stack alignment, even
-// if user taken care with stack alignment, but many versions
-// of libc on linux call main() with the wrong initial stack
-// alignment the result that the code is now pessimally aligned
-// instead of having a 50% chance of being correct.
-//
-#if OGRE_ARCH_TYPE != OGRE_ARCHITECTURE_64
-
-#define __OGRE_SIMD_ALIGN_STACK()                                   \
-    {                                                               \
-        /* Use alloca to allocate some memory on the stack.  */     \
-        /* This alerts gcc that something funny is going on, */     \
-        /* so that it does not omit the frame pointer etc.   */     \
-        (void)__builtin_alloca(16);                                 \
-        /* Now align the stack pointer */                           \
-        __asm__ __volatile__ ("andl $-16, %esp");                   \
-    }
-
-#else // 64
-#define __OGRE_SIMD_ALIGN_STACK()                                   \
-    {                                                               \
-        /* Use alloca to allocate some memory on the stack.  */     \
-        /* This alerts gcc that something funny is going on, */     \
-        /* so that it does not omit the frame pointer etc.   */     \
-        (void)__builtin_alloca(16);                                 \
-        /* Now align the stack pointer */                           \
-        __asm__ __volatile__ ("andq $-16, %rsp");                   \
-    }
-#endif //64
+#elif OGRE_CPU == OGRE_CPU_X86 && (OGRE_COMPILER == OGRE_COMPILER_GNUC || OGRE_COMPILER == OGRE_COMPILER_CLANG) && (OGRE_ARCH_TYPE != OGRE_ARCHITECTURE_64)
+// mark functions with GCC attribute to force stack alignment to 16 bytes
+#define __OGRE_SIMD_ALIGN_ATTRIBUTE __attribute__((force_align_arg_pointer))
 
 #elif defined(_MSC_VER)
 // Fortunately, MSVC will align the stack automatically
+#define __OGRE_SIMD_ALIGN_ATTRIBUTE
+
+#else
+#define __OGRE_SIMD_ALIGN_ATTRIBUTE
 
 #endif
 
